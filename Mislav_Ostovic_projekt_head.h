#ifndef MISLAV_OSTOVIC_PROJEKT_HEAD_H
#define MISLAV_OSTOVIC_PROJEKT_HEAD_H

typedef struct proizvod {
	char ime[30];
	float cijena;
	int kolicina;
}PROIZVOD;


void stvaranjeDatoteke();
void unosPodataka();
void brojacPodataka();
void ispisPodataka();
int SortMin(const void*, const void*);
int SortMax(const void*, const void*);
void brisanjePodatka();
void brisanjeDatoteke();


#endif //MISLAV_OSTOVIC_PROJEKT_HEAD_H#pragma once
